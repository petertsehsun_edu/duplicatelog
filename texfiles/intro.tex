\section{Introduction}
\label{sec:intro}


Software logs are widely used in software systems to record system execution behaviors. Developers use the generated logs to assist in various tasks, such as debugging~\cite{Yuan:2011:ISD:1950365.1950369, Yuan:2010:SED:1736020.1736038, Fu:2014:DLE:2591062.2591175}, testing~\cite{Chen:2017:ALT:3103112.3103144, jacktool, jackase2018}, program comprehension~\cite{Hassan:2008:ICS:1368088.1379445, Shang:2014:ULL:2705615.2706065}, system verification~\cite{Busany:2016:BLA:2884781.2884805, DBLP:journals/jacic/BarringerGHS10}, and performance analysis~\cite{Chen:2016:CHD:2950290.2950303, kundi_icpe_2018}. A logging statement (i.e., code that generates a log) contains a static message, to-be-recorded variables, and log verbosity level. As an example, a logging statement may be written as {\em logger.error(``Interrupted while waiting for fencing command: '' + cmd);}. In this example, the static text message is {\em ``Interrupted while waiting for fencing command: ''}, and the dynamic message is from the variable {\em cmd}, which records the command that is being executed. The logging statement is at the {\em error} level, which is the level for recording failed operations~\cite{log4j}.


Even though developers have been analyzing logs for decades~\cite{Kabinna:2016:LLM:2901739.2901769}, there exists no industrial standard on how to write logging statements~\cite{Fu:2014:DLE:2591062.2591175, 7202961}. Prior studies often focus on recommending where logging statements should be added into the code (i.e., {\em where-to-log})~\cite{Zhu:2015:LLH:2818754.2818807, Zhao:2017:LFA:3132747.3132778}, and what information should be added in logging statements (i.e., {\em what-to-log})~\cite{Shang:2014:ULL:2705615.2706065, Yuan:2011:ISD:1950365.1950369, aseLog2018}. A few recent studies~\cite{log_pattern_ICSE2017, mehran_emse_2018} aim to detect potential problems in logging statements. However, these studies often only consider the appropriateness of one single logging statement as an individual item; while logs are typically analyzed in tandem~\cite{Yuan:2011:ISD:1950365.1950369, Chen:2016:CHD:2950290.2950303}. In other words, we consider that the appropriateness of a log is also influenced by other logs that are generated in system execution.

In particular, an intuitive case of such influence is duplicate logs, i.e., multiple logs that have the same text message. Even though each log itself may be impeccable, duplicate logs may affect developers' understanding of the dynamic view of the system. For example, as shown in Figure~\ref{fig:M_example_intro}, there are two logging statements in two different {\em catch} blocks, which are associated with the same {\em try} block. These two logging statements have the same static text message and do not include any other error-diagnostic information. Thus, developers cannot easily distinguish what is the occurred exception when analyzing the produced logs.
%we find that developers copied and pasted a log statement from {\em removeGroup} to {\em updateGroup} (i.e., causes duplicate logs), but the copied log statement in {\em updateGroup} still contains the static text {\em ``remove group''}, which should be updated in the new context. %Although the two methods have syntactic similarities, the semantic are different (i.e., update v.s. remove).
Since developers rely on logs for debugging and program comprehension~\cite{Shang:2014:ULL:2705615.2706065}, such duplicate logging statements may negatively affect developers' activities in maintenance and quality assurance. %On the other hand, developers may also intentionally inject such duplicate logs to accomplish tasks that are not easily supported by the current logging libraries.

 \begin{figure}
 \centering
 %\begin{lstlisting}
%public boolean updateGroup(final CloudianGroup group) {
 %   ...
  %  } catch (final IOException e) {
   %     LOG.error("Failed to remove group due to:", e);
    %    checkResponseTimeOut(e);
    %}
    %...
%}
 %\end{lstlisting}
%\includegraphics[width=0.9\linewidth]{figures/M_example_intro.pdf}

\begin{lstlisting}
...
} catch (AlreadyClosedException closedException) {
       s_logger.warn("Connection to AMQP service is lost.");
} catch (ConnectException connectException) {
       s_logger.warn("Connection to AMQP service is lost.");
}
...
\end{lstlisting}
\vspace{-0.3cm}
 \caption{An example of duplicate logging code smell that we detected in CloudStack. The duplicate logging statements in the two {\em catch} blocks contain insufficient information (e.g., no exception type or stack trace) to distinguish what may be the occurred exception.}
 \vspace{-0.3cm}
 \label{fig:M_example_intro}
 \end{figure}

 %\peter{updated, please check}\ian{I think we need a different example, to actually show "duplicate" and say this make things confusing. The example now is more about inconsistency. Readers of the paper just get started into this concept, we shouldn't jump into a pattern like that.}\todo{Zhenhao, can you find an example of IC and paste it here? Find one that is not too big} \zhenhao{Please see the comment here}
%CloudStack org.apache.cloudstack.mom.rabbitmq.RabbitMQEventBus.subscribe()
%\begin{lstlisting}
%   } catch (AlreadyClosedException closedException) {
%        s_logger.warn("Connection to AMQP service is lost. Subscription:" + queueName + " will be active after reconnection");
%   } catch (ConnectException connectException) {
%        s_logger.warn("Connection to AMQP service is lost. Subscription:" + queueName + " will be active after reconnection");
%   } catch (Exception e) {
%         throw new EventBusException("Failed to subscribe to event due to " + e.getMessage());
%   }
%\end{lstlisting}

%We find that log statements with the same static text messages, may appear in different contexts (e.g., surrounding code or methods with syntactic or semantic differences). Since developers rely on such log messages for debugging and program comprehension~\cite{Shang:2014:ULL:2705615.2706065}, having duplicate logs may negatively impact developers' activities in maintenance and quality assurance. However,


 \begin{figure}
 \centering
\includegraphics[width=\columnwidth]{figures/duplog_overall.pdf}
\vspace{-0.4cm}
 \caption{The overall process of our study. The term ``duplicate logging statements'' is referred as ``duplicate logs'' for simplification.}
 \vspace{-0.6cm}
 \label{fig:overall}
 \end{figure}

%In order to understand the phenomenon of log duplication in depth,
%\ian{inconsistent between present and past tense, stick to one.} \zhenhao{converted to past tense}
To help developers improve logging practices, in this paper, we focus on studying duplicate logging statements in the source code. We conducted a manual study on four large-scale open source systems, namely Hadoop, CloudStack, ElasticSearch, and Cassandra. We first used static analysis to identify all duplicate logging statements, which are defined as two or more logging statements that have the same static text message. We then manually studied all the (over 3K) identified duplicate logging statements and uncovered five patterns of {\em duplicate logging code smells}. We follow prior code smell studies~\cite{budgen2003software, fowler1999refactoring}, and consider duplicate logging code smell as a ``surface indication that usually corresponds to a deeper problem in the system''. However, {\em not} all of the duplicate logging code smell are problematic and require fixes (i.e., {\em problematic duplicate logging code smells}). In particular, context (e.g., surrounding code and usage scenario of logging) may play an important role in identifying fixing opportunities. Hence, we further categorized duplicate logging code smells into {\em problematic} or {\em justifiable} cases. In addition to our manual analysis, we sought confirmation from developers on the manual analysis result: For the problematic duplicate logging code smells, we reported them to developers for fixing. For the justifiable ones, we communicated with developers for discussion (e.g., emails or posts on developers' forums).

We implemented a static analysis tool, \tool, to automatically detect {\em problematic} duplicate logging code smells. \toolS leverages the findings from our manual study, including the uncovered patterns of duplicate logging code smells and the categorization on problematic and justifiable cases.
We evaluated \toolS on six systems: four are from the manual study and two are additional systems (Camel and Wicket). We also applied \toolS on the updated versions of the four manually studied systems. The evaluation shows that the uncovered patterns of the duplicate logging code smells also exist in the two additional systems, and duplicate logging code smells may be introduced over time. An automated approach such as \toolS can help developers avoid duplicate logging code smells as systems evolve.

In total, we reported 82 instances of duplicate logging code smell to developers and all the reported instances are fixed. %Our fix suggestions are all accepted by developers.


Figure~\ref{fig:overall} shows the overall process of this paper.

%that developers are likely to introduce such logging code smells as systems evolve. Our approach can help developers ensure %DLFinder can help developers avoid duplicate logging code smells as the system evolves.%\jinqiu{TODO, say one or two sentences on evaluation}



%Our manual study starts with extracting {\em duplicate logging statements}, which are defined as two or more logging statements that have the same static text message. Among {\em all} of the extracted duplicate logging statements from the four systems, we manually identify {\em duplicate logging code smells} and conclude patterns of such code smells. We follow prior code smell studies~\cite{budgen2003software, fowler1999refactoring}, and define duplicate logging code smell as a ``surface indication that usually corresponds to a deeper problem in the system''. However, {\em not} all of the duplicate logging code smell are problematic and require fixes (i.e., {\em problematic duplicate logging code smells}). In particular, context (e.g., surrounding code, usage scenario of logging) may play an important role in identifying fixing opportunities. Hence, we further categorize duplicate logging code smells into {\em problematic} or {\em justifiable} cases. In addition to our manual analysis, we seek for confirmation from developers on the manual analysis result: For problematic duplicate logging code smells, we reported them to developers for fixing. For justifiable ones, we communicated with developers for discussion (e.g., emails or posts on developers' forums).

%\jinqiu{Updated flow on the old version, delete the para. above}To help developers improve logging practices, in this paper, we focus on studying duplicate logging statements in the source code. We define duplicate logging statements as two or more logging statements that have the same static text message. We conduct a manual study on four large-scale open source systems, namely Hadoop, CloudStack, ElasticSearch, and Cassandra to uncover duplicate logging code smells. Similar to prior code smell studies~\cite{budgen2003software, fowler1999refactoring}, we define duplicate logging code smells as a {\em ``surface indication that usually corresponds to a deeper problem in the system''}. We first extract all duplicate logging statements from the studied systems. Afterwards, we manually uncover patterns of duplicate logging code smells from all the extracted duplicate logging statements. Similar to duplicate code~\cite{kapser2006a}, we believe that not every duplicate log will be problematic and require fixes (i.e., duplicate logging code smell). In particular, the context (i.e., surrounding code) may play an important role in identifying fixing opportunities. Therefore, we further categorize the uncovered duplicate logging code smells into problematic and justifiable cases. We also contact developers for feedback on our manual study results.



%This semi-automated approach allows us to focus our manual study effort on potential problematic patterns of duplicate logging code smells, instead of manually studying all duplicate logs.



%Similar to duplicate code~\cite{kapser2006a}, we believe that not every duplicate log will be a problem that requires refactoring (i.e., {\it duplicate logging code smell}). In particular, the context of the surrounding code may play an important role in identifying refactoring opportunities. Therefore, we use a semi-automated approach to identify {\it potential} instances of duplicate logging code smell. First, we take a statistical sample of duplicate logs. Second, we manually study the \textbf{sampled} duplicate logs to uncover patterns of duplicate logging code smells. These uncovered patterns distill all the duplicate logging code smells that we find, which may cause problems in log understanding. %distinguish the instances of duplicate logging code smell from the remaining benign duplicate logs.

%Third, we implement a static analysis tool, \tool, to automatically identify \textbf{all} the duplicate logging code smell instances that are related to the uncovered patterns. This semi-automated approach allows us to focus our manual study effort on potential problematic patterns of duplicate logging code smells, instead of manually studying all duplicate logs.

%accurately? identify instances of duplicate logging code smell without manually studying every duplicate log.%duplicate logging code smells that follow the five resulting patterns.
%To study duplicate logs, we first automatically identify all duplicate logs in four large-scale open source systems (i.e., Hadoop, CloudStack, ElasticSearch, and Cassandra). Then, we manually study a statistical sample of duplicate logs and uncover five types of duplicate logging code smells that may cause problems in log understanding.

%\jinqiu{Not every duplicate log code smell has the same level of impact: some are real problems that require fixes; others may not be harmful.}


%We further manually classify each instance of the detected duplicate logging code smell into sub-categories based on the context of the logging code (e.g., control flow or semantics of the surrounding code).~\jinqiu{sometimes it is syntactically similar, or code structure or here control flow, may use one consistent term} We identify some sub-categories of each pattern that are more likely to have a higher impact (i.e., requires refactoring). We then send inquiries to developers for verifying our manual study result and the potential impact of the uncovered logging smells (i.e., whether or not requires refactoring). Finally, we summarize the feedback that we received and enhanced \toolS accordingly.  %implement a static analysis tool called \tool, which provide suggestions to developers on how to refactor duplicate logging code smells.% anti-patterns.
In summary, this paper makes the following contributions: %that we receive into a static code checker that we implement. %Once the anti-patterns are derived, we then manually study the genealogy
%~\jinqiu{The last bullet can be the third one? The findings first, then the tool and result.}
\vspace{-0.1cm}
\begin{itemize} \itemsep 0em
  \item We uncovered five patterns of duplicate logging code smells through an extensive manual study on over 3K duplicate logging statements.%This is the first study on helping developers refactor logging code.

  %\item We find that there is a non-negligible number of duplicate logging statements in the studied systems (6\%--20\% of all log lines). %Duplicate logs also have a similar level of semantic information (i.e., in terms of number of words) compared to non-duplicate logs (both have a median of 6 to 8 words). The finding shows that duplicate logs are often not generic logs with only a few words.

  %\item We find that although 60\% of duplicate logs are related to duplicate code, 40\% of them are not.
  \item We presented a categorization of duplicate logging code smells (i.e., problematic or justifiable), based on both our manual assessment (i.e., studying the logging statement and its surrounding code) and developers' feedback.

%  \item We manually assessed every logging code smell instance, whether it is problematic or justifiable, based our understanding (i.e., studying the logging statement and its surrounding code) and developers' feedback. %We further categorize each pattern of duplicate logging code smell into sub-categories based on its potential impact.

  \item We proposed \tool, a static analysis tool that integrates our manual study result and developers' feedback to detect problematic duplicate logging code smells. We evaluated \toolS for both the accuracy and generalization (i.e., on new systems and on the newer versions as systems evolve).
%  \item We integrated our manual study result and developers' feedback into our static analysis tool, \tool, which detects problematic duplicate logging code smells. We also evaluated \toolS for both the accuracy and generalization (i.e., on new systems and on the newer versions as systems evolve).%The tool has already provided useful suggestions (31 code smell instances are reported and accepted) to developers of our studied systems.

  %\item We send inquiries to developers regarding


  %\item Based on our manual study on duplicate logs and the feedback that we received from developers, we implement a static analysis tool, \tool, that can help developers with refactoring five types of manually-uncovered duplicate logging code smells. %. For three out of five types of duplicate logging code smells, %We reported three types of code smells % instances logging code smells

  \item We reported 82 instances of problematic duplicate logging code smells to developers (\toolS is able to detect 72 of them), and all of the reported instances are fixed.

  %\item We make all the data publicly available, e.g., the annotated duplicate logging statements, our manual analysis result, and developers' feedback\footnotemark[\value{footnote}].

%\footnote{We are still waiting for developers' feedback on one reported code smell instance.}.




\end{itemize}


%In short, prior studies on detecting logging problems did not study the reasons for false positives. Based on our manual study result on the false positives and the feedback that we received from developers, we find that the impact of logging code smells is highly correlated with both {\em the semantic and syntactic context} (e.g., structure and the functionality) of the surrendering code. Future studies should consider the code context information when refactoring or detecting logging code smells.


%on logging code refactoring and detection should consider the .

%Different from prior studies on detecting logging problems, we find that many logging code smells may not be a problem. Based on our manual study result and the feedback that we received from developers, we find that both of the semantic and syntactic context of the code (i.e.,)


%Prior studies on software logging usually focus on helping developers with {\em ``where-to-log''} and {\em ``what-to-log''}. Since there is performance overhead associated with logging, developers cannot blindly add log lines in the source code~\cite{Fu:2014:DLE:2591062.2591175}. To assist developers with ``where-to-log'', researchers help developers by recommending where the log lines should be added in the code~\cite{Zhu:2015:LLH:2818754.2818807, Zhao:2017:LFA:3132747.3132778}.


%Prior studies on software logging focus on three general logging challenges: {\em ``where-to-log''}, {\em ``what-to-log''}, and {\em ``how-to-log''}. Since there is performance overhead associated with logging, blindly adding log lines in the code is not desirable~\cite{Fu:2014:DLE:2591062.2591175}. To assist developers with ``where-to-log'', researchers help developers by recommending where the log lines should be added in the code~\cite{kundi_icpe_2018, Zhu:2015:LLH:2818754.2818807, Zhao:2017:LFA:3132747.3132778}. The quality of logs is on helping developers with {\em ``where-to-log''} and {\em ``what-to-log''}.




%Since there is performance overhead associated with logging, blindly adding log lines in the code is not desirable~\cite{Fu:2014:DLE:2591062.2591175}. Hence, to assist ``where-to-log'', researchers helps developer by recommending where the log lines should be added in the code~\cite{kundi_icpe_2018, Zhu:2015:LLH:2818754.2818807, Zhao:2017:LFA:3132747.3132778}. The dynamic information (i.e., value of the dynamic variable) that is recorded in the log helps developers with program understanding and debugging.




%There is performance overhead associated with logging, so excessive logging is undesired in production systems~\cite{Fu:2014:DLE:2591062.2591175}. %Excessive logging introduces overhead to the system

\phead{Paper organization.} The rest of the paper is organized as follows. Section~\ref{sec:prestudy} describes how we prepare the data for manual study (i.e., duplicate logging statements) and the studied systems. Section~\ref{sec:manual} discusses the process and the results of our manual study, and also developers' feedback on our results. Section~\ref{sec:detection} discusses the implementation details of \tool. Section~\ref{sec:results} evaluates \toolS for both the accuracy and generalization. Section~\ref{sec:threats} discusses the threats to validity of our study. Section~\ref{sec:related} surveys related work. Finally, Section~\ref{sec:conclusion} concludes the paper.



%how we manually study the duplicate logging statements the patterns of duplicate logging code smells that we uncovered based on our manual study. %shows the approach %~\jinqiu{the semi-automated approach for finding instances of interest}
%and the result of our manual analysis on duplicate logs.
%Section~\ref{sec:userstudy} further manually categorizes each uncovered pattern into problematic and justifiable cases, and we verified our results by sending inquiries to developers.
%Section~\ref{sec:detection} discusses how we implement an automated static analysis tool to detect and prioritize the studied patterns. Section~\ref{sec:threats} discusses the threats to validity of our study. Section~\ref{sec:related} surveys related work. Finally, Section~\ref{sec:conclusion} concludes the paper.



%A large number of prior studies~\cite{kim2005, ekwa2007, gabel2008, baxter1998, kamiya2002, roy07, nikos_icse_2018, emdenwcre02} focus on refactoring different types of code smells, such as code clones. These code smells may be an indication of problems that can impact software quality, and cause additional challenges during debugging and maintenance. Log lines, on the other hand, are also part of the source code, and are often used to show the dynamic view of the system. However, prior code smell studies only focus on refactoring the static view of the software, and do not consider potential code smells that may exist in log lines.

%Prior studies focus on refactoring such as code smells and code clones. These smells can cause comprehension problems. Log is part of code, but prior studies rarely look at them.

%There are where to log, what to log, but not many how to log to assist developers with logging code. This study highlight logging code smells.

%We focus on duplicate log smells in this paper


\begin{comment}
duplicate code may cause problem with code comprehension, maintenance. people use refactoring to help resolve duplicate code and improve code readability. even if some of the code is refactored, duplicate code may still cause runtime problems in debugging and understanding.

However, there are problems with log understand that are caused by duplicate code or code clone.

-------------------------------

duplicate code may cause problem with code comprehension, maintenance. people use refactoring to help resolve duplicate code and improve code readability. even if some of the code is refactored, duplicate code may still cause runtime problems in debugging and understanding.


Give an example of problematic log lines (say it's detected by our approach and fixed by developers). Developers copied the code but did not change the message of the logs, and can potentially mislead developers with understanding.

Existing research on log focus on where to log and how to log, but not on confusing logs. Such problematic logs may impose challenges in log analysis, log understanding etc. We are the first work.

We conduct a study on the prevalence of duplicate logs and a manual study to understand the cause and impact of duplicate logs. Finally, we detect the log problems.
\end{comment}


%Source code duplication, commonly known as code cloning, is considered an obstacle to software maintenance because changes to a cloned region often require consistent changes to other regions of the source code. Research has provided evidence that the elimination of clones may not always be practical, feasible, or cost-effective.
